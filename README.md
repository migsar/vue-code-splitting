# vue-code-splitting

Code splitting test and demo app.

We are interested in the following:
- Lazy loading
- Asset independence

Lazy loading means we won't fetch components until those are required.
Asset independence is a derivation from lazy loading, we need to be sure that there is no access to assets before they are loading, yeah, it means no pre-fetching.

---

The application will consist in a colors app, hierarchically set to prove code-splitting feature:

```
 home
   red
     orange
     yellow
   blue
     green
     yellow
```
We expect:
  - there is an app chunk that is initially loaded and does not contain data from any other chunk
  - red and orange are in the same chunk
  - blue and green are in the same chunk
  - yellow has it's own chunk

We use a common component on purpose to see how it is handled, we care about CSS and images as well.

## Instructions

Instructions are for yarn:

1. Add [dynamic import](https://babeljs.io/docs/en/babel-plugin-syntax-dynamic-import/) to Babel

```bash
yarn add --dev @babel/plugin-syntax-dynamic-import
```

In `babel.config.js`:

```javascript
module.exports = {
  ...
  plugins: [
    '@babel/plugin-syntax-dynamic-import'
  ]
}
```

2. Build project

```
yarn build
```

3. Use grep on `/dist` and `/dist/css` to see what is included and what is not

```
grep -ro red css/app.14890e85.css
```